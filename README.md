# 另一个蘑菇仓库

## 介绍
软件基础第四次作业

#### 软件功能及描述
实现一个简易命令行计算器程序。输入数字和算法后能进行四则运算加减乘除，计算结果保留到小数点后2位。
程序要求能处理用户的输入，判断异常。
程序支持可以由用户自行选择加、减、乘、除运算。


#### 文件列表及其相关说明

v0.1 为初始版本； v0.2 为新版本。

#### 例程运行及其相关结果

加法：![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/125611_6588cecc_9901302.png "屏幕截图.png")

减法：![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/125647_3f553fd6_9901302.png "屏幕截图.png")

乘法：![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/125718_99302d6e_9901302.png "屏幕截图.png")

除法：![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/125753_5758b5b7_9901302.png "屏幕截图.png")

分母为零：![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/130336_fd9ae9f1_9901302.png "屏幕截图.png")

输入出错：![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/130429_4e2a367e_9901302.png "屏幕截图.png")